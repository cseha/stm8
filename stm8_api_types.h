/**
 * Copyright 2022
 * 
 * @author: Mark Cseharovszky
 * @file:   stm8_api_types.h
 *  
 * @brief:  Header file for STM8 GPIO driver source
 *          This file is constantly growing as I work on STM8 driver
 *          development.
 */

#include <stdint.h>

#ifndef STM8_API_TYPES_H_INCLUDED
#define STM8_API_TYPES_H_INCLUDED

#define STM_OK (uint8_t)0x00

typedef enum {
   null_pointer = (uint8_t)0x01,
   generic_error,
   invalid_parameter,
   invalid_gpio_direction,
   invalid_gpio_level,
   invalid_gpio_mode,
   invalid_gpio_isr_or_speed
} stm_err_t; /* if errorcode is not defined here multiple errors occured in function */

#define STM_NULLPTR                    null_pointer
#define STM_NOK                        generic_error
#define STM_INVALID_PARAMETER          invalid_parameter
#define STM_INVALID_GPIO_DIRECTION     invalid_gpio_direction
#define STM_INVALID_GPIO_LEVEL         invalid_gpio_level
#define STM_INVALID_GPIO_MODE          invalid_gpio_mode
#define STM_INVALID_GPIO_ISR_OR_SPEED  invalid_gpio_isr_or_speed

#endif // STM8_API_TYPES_H_INCLUDED