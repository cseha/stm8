 /**
 * Copyright 2022
 * 
 * @author: Mark Cseharovszky
 * @file:   stm8_register.h
 *  
 * @brief:  Header file for accessing STM8 registers
 *          This file is constantly growing as I work on STM8 driver
 *          development.
 */

#ifndef STM8_REGISTER_H_INCLUDED
#define STM8_REGISTER_H_INCLUDED

#include <stdio.h>
#include <stdint.h>

/**---------------------------------------------------------------------------------------
 *                                  STM8 PRODUCT LINES
----------------------------------------------------------------------------------------*/

/* the microcontroller type should be defined in the main file beginning */
#if defined            STM8S105C4 \
            || defined STM8S105C6 \
            || defined STM8S105K4 \
            || defined STM8S105K6 \
            || defined STM8S105S4 \
            || defined STM8S105S6
#define STM8S105x4_6
#endif

#if defined            STM8S103F2 \
            || defined STM8S103F3 \
            || defined STM8S103K3
#define STM8S105F2_x3
#endif

/**---------------------------------------------------------------------------------------
 *                                  PERIPHERIALS
----------------------------------------------------------------------------------------*/

/**---------------------------------------------------------------------------------------
 *                                  TIMERS
----------------------------------------------------------------------------------------*/

#if defined STM8S105x4_6 || defined STM8S105F2_x3

#define TIM1_BaseAddress 0x5250

typedef struct {
   uint8_t CR1;
   uint8_t CR2;
   uint8_t SMCR;
   uint8_t ETR;
   uint8_t IER;
   uint8_t SR1;
   uint8_t SR2;
   uint8_t EGR;
   uint8_t CCMR1;
   uint8_t CCMR2;
   uint8_t CCMR3;
   uint8_t CCMR4;
   uint8_t CCER1;
   uint8_t CCER2;
   uint8_t CNTRH;
   uint8_t CNTRL;
   uint8_t PSCRH;
   uint8_t PSCRL;
   uint8_t ARRH;
   uint8_t ARRL;
   uint8_t RCR;
   uint8_t CCR1H;
   uint8_t CCR1L;
   uint8_t CCR2H;
   uint8_t CCR2L;
   uint8_t CCR3H;
   uint8_t CCR3L;
   uint8_t CCR4H;
   uint8_t CCR4L;
   uint8_t BKR;
   uint8_t DTR;
   uint8_t OISR;
} TIM1_TypeDef;

#define TIM1 ((TIM1_TypeDef*)TIM1_BaseAddress)

#endif

#if defined STM8S105x4_6 || defined STM8S105F2_x3

#define TIM2_BaseAddress 0x5300

typedef struct {
   uint8_t CR1;
   uint8_t IER;
   uint8_t SR1;
   uint8_t SR2;
   uint8_t EGR;
   uint8_t CCMR1;
   uint8_t CCMR2;
   uint8_t CCMR3;
   uint8_t CCER1;
   uint8_t CCER2;
   uint8_t CNTRH;
   uint8_t CNTRL;
   uint8_t PSCR;
   uint8_t ARRH;
   uint8_t ARRL;
   uint8_t CCR1H;
   uint8_t CCR1L;
   uint8_t CCR2H;
   uint8_t CCR2L;
   uint8_t CCR3H;
   uint8_t CCR3L;
} TIM2_TypeDef;

#define TIM2 ((TIM1_TypeDef*)TIM2_BaseAddress)

#endif

#ifdef STM8S105x4_6

#define TIM3_BaseAddress 0x5320

typedef struct {
   uint8_t CR1;
   uint8_t IER;
   uint8_t SR1;
   uint8_t SR2;
   uint8_t EGR;
   uint8_t CCMR1;
   uint8_t CCMR2;
   uint8_t CCER1;
   uint8_t CNTRH;
   uint8_t CNTRL;
   uint8_t PSCR;
   uint8_t ARRH;
   uint8_t ARRL;
   uint8_t CCR1H;
   uint8_t CCR1L;
   uint8_t CCR2H;
   uint8_t CCR2L;
} TIM3_TypeDef;

#define TIM3 ((TIM1_TypeDef*)TIM3_BaseAddress)

#endif

#if defined STM8S105x4_6 || defined STM8S105F2_x3

#define TIM4_BaseAddress 0x5340

typedef struct {
   uint8_t CR1;      /* Control register           */
   uint8_t IER;      /* Interrput enable register  */
   uint8_t SR;       /* Status register            */
   uint8_t EGR;      /* Event generation register  */
   uint8_t CNTR;     /* Counter value              */
   uint8_t PSCR;     /* Prescaler value            */
   uint8_t ARR;      /* Auto-reload value          */
} TIM4_TypeDef;

#define TIM4 ((TIM1_TypeDef*)TIM4_BaseAddress)

#endif

/**---------------------------------------------------------------------------------------
 *                                  GPIO
----------------------------------------------------------------------------------------*/

typedef struct {
   volatile uint8_t ODR;          /* 0x00: output data register    */
   volatile uint8_t IDR;          /* 0x01: input data register     */
   volatile uint8_t DDR;          /* 0x02: data dircetion register */
   volatile uint8_t CR1;          /* 0x03: control register 1      */
   volatile uint8_t CR2;          /* 0x04: control register 2      */
} GPIO_TypeDef;

#define GPIOA_BaseAddress (0x005000)
#define GPIOB_BaseAddress (0x005005)
#define GPIOC_BaseAddress (0x00500A)
#define GPIOD_BaseAddress (0x00500F)
#define GPIOE_BaseAddress (0x005014)
#define GPIOF_BaseAddress (0x005019)

#define GPIOA ((GPIO_TypeDef*) GPIOA_BaseAddress)
#define GPIOB ((GPIO_TypeDef*) GPIOB_BaseAddress)
#define GPIOC ((GPIO_TypeDef*) GPIOC_BaseAddress)
#define GPIOD ((GPIO_TypeDef*) GPIOD_BaseAddress)
#define GPIOE ((GPIO_TypeDef*) GPIOE_BaseAddress)
#define GPIOF ((GPIO_TypeDef*) GPIOF_BaseAddress)

#endif // STM8_REGISTER_H_INCLUDED