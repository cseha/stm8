/**
 * Copyright 2022
 * 
 * @author: Mark Cseharovszky
 * @file:   stm8_gpio_driver.h
 *  
 * @brief:  Header file for STM8 GPIO driver source
 *          This file is constantly growing as I work on STM8 driver
 *          development.
 */
 
#ifndef STM8_GPIO_H_INCLUDED
#define STM8_GPIO_H_INCLUDED

#include "stm8_register.h"
#include "stm8_api_types.h"

/**---------------------------------------------------------------------------------------
 * Type definitions
----------------------------------------------------------------------------------------*/

typedef enum {
   GPIO_PIN_0 = (uint8_t)0x00,
   GPIO_PIN_1,
   GPIO_PIN_2,
   GPIO_PIN_3,
   GPIO_PIN_4,
   GPIO_PIN_5,
   GPIO_PIN_6,
   GPIO_PIN_7
} gpio_pin_t;

typedef enum {
   GPIO_DIRECTION_INPUT = (uint8_t)0x00,
   GPIO_DIRECTION_OUTPUT
} gpio_direction_t;

typedef enum {
   GPIO_LEVEL_LOW = (uint8_t)0x00,
   GPIO_LEVEL_HIGH
} gpio_level_t;

typedef enum {
   GPIO_INPUT_FLOATING = (uint8_t)0x00,
   GPIO_INPUT_PULLUP,
   GPIO_OUTPUT_OPENDRAIN,
   GPIO_OUTPUT_PUSHPULL
} gpio_mode_t;

typedef enum {
   GPIO_INPUT_ISR_DISABLE = (uint8_t)0x00,
   GPIO_INPUT_ISR_ENABLE,
   GPIO_OUTPUT_SPEED_2MHZ,
   GPIO_OUTPUT_SPEED_10MHZ
} gpio_isr_or_speed_t;

typedef struct {
   gpio_direction_t direction;
   gpio_mode_t mode;
   gpio_isr_or_speed_t isr_or_speed;
} gpio_config_t;

typedef struct {
   GPIO_TypeDef *port;
   gpio_pin_t pin;
   gpio_config_t config;
} gpio_handle_t;

/**---------------------------------------------------------------------------------------
 * MACRO definitions
----------------------------------------------------------------------------------------*/

#define GPIO_SET_DIRECTION_INPUT(port, pin)        ((port)->DDR &= ~(1U<<(pin)))
#define GPIO_SET_DIRECTION_OUTPUT(port, pin)       ((port)->DDR |= 1U<<(pin))

#define GPIO_SET_PIN_LOW(port, pin)                ((port)->ODR &= ~(1U<<(pin)))
#define GPIO_SET_PIN_HIGH(port, pin)               ((port)->ODR |= 1U<<(pin))

#define GPIO_SET_FLOATING_OPENDRAIN(port, pin)     ((port)->CR1 &= ~(1U<<(pin)))
#define GPIO_SET_PULLUP_PUSHPULL(port, pin)        ((port)->CR1 |= 1U<<(pin))

#define GPIO_SET_ISR_DISABLE_SPEED_2MHZ(port, pin) ((port)->CR2 &= ~(1U<<(pin)))
#define GPIO_SET_ISR_ENABLE_SPEED_10MHZ(port, pin) ((port)->CR2 |= 1U<<(pin))

#define GPIO_TOGGLE_PIN(port, pin)                 ((port)->ODR ^= 1U<<(pin))

/**---------------------------------------------------------------------------------------
 * Public function definitions
 *--------------------------------------------------------------------------------------*/

/**
 * @brief   Initializes GPIO pin with configuration assigned to gpio_handle_t.
 * 
 * @param   handler     pointer to gpio_handle_t
 * @return  stm_err_t:  STM_OK Success
 *                      STM_INVALID_PARAMETER Incorrect parameter
 */
stm_err_t gpio_init(gpio_handle_t* handler);

/**
 * @brief   Sets input mode to floating or pullup and updates information
 *          in gpio_handler_t(TODO).
 * 
 * @param   handler     pointer to gpio_handle_t
 * @param   input_mode  select from gpio_direction_t
 * @return  stm_err_t:  STM_OK Success
 *                      STM_INVALID_PARAMETER Incorrect parameter
 */
stm_err_t gpio_set_direction(gpio_handle_t* handler, gpio_direction_t direction);

/**
 * @brief   In input direction sets floating or pull up mode, in output direction sets
 *          opend drain or push pull mode; and updates information in gpio_handler_t(TODO).
 *
 * @param   handler     pointer to gpio_handle_t
 * @param   input_mode  select from gpio_mode_t
 * @return  stm_err_t:  STM_OK Success
 *                      STM_INVALID_PARAMETER Incorrect parameter
 */
stm_err_t gpio_set_mode(gpio_handle_t* handler, gpio_mode_t mode);

/**
 * @brief   In input direction enables or disables external interrupts, in output
 *          direction sets output speed; and updates information in gpio_handler_t(TODO).
 * 
 * @param   handler           pointer to gpio_handle_t
 * @param   input_interrupt   select from gpio_isr_or_speed_t
 * @return  stm_err_t:        STM_OK Success
 *                            STM_INVALID_PARAMETER Incorrect parameter
 */
stm_err_t gpio_set_isr_or_speed(gpio_handle_t* handler, gpio_isr_or_speed_t isr_or_speed);

/**
 * @brief TODO
 * 
 * @param handler 
 * @return stm_err_t 
 */
stm_err_t gpio_toggle_pin(gpio_handle_t* handler);

/**
 * @brief TODO
 * 
 * @param handler 
 * @param level 
 * @return stm_err_t 
 */
stm_err_t gpio_set_level(gpio_handle_t* handler, gpio_level_t level);

#endif // STM8_GPIO_H_INCLUDED