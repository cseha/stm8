/**
 * Copyright 2022
 * 
 * @author: Mark Cseharovszky
 * @file:   stm8_gpio_driver.c
 * 
 * @brief:  Source file for STM8 GPIO peripherial.
 *          This file is constantly growing as I work on STM8 driver
 *          development.
 */

#define STM8_GPIO_DRIVER_SOURCE

#include "stm8_gpio.h"

stm_err_t gpio_init(gpio_handle_t* handler)
{
   stm_err_t result = STM_OK;

   if(handler == NULL)
   {
      return STM_NULLPTR;
   }

   gpio_set_direction(handler, handler->config.direction);
   gpio_set_mode(handler, handler->config.mode);
   gpio_set_isr_or_speed(handler, handler->config.isr_or_speed);

   return result;
}

stm_err_t gpio_set_direction(gpio_handle_t* handler, gpio_direction_t direction)
{
   stm_err_t result = STM_OK;

   if(handler == NULL)
   {
      return STM_NULLPTR;
   }

   if (direction == GPIO_DIRECTION_INPUT)
   {
      GPIO_SET_DIRECTION_INPUT(handler->port, handler->pin);
   }
   else if (direction == GPIO_DIRECTION_OUTPUT)
   {
      GPIO_SET_DIRECTION_OUTPUT(handler->port, handler->pin);
   }
   else
   {
      result = STM_INVALID_GPIO_DIRECTION;
   }

   return result;
}

stm_err_t gpio_set_mode(gpio_handle_t* handler, gpio_mode_t mode)
{
   stm_err_t result = STM_OK;

   if(handler == NULL)
   {
      return STM_NULLPTR;
   }

   if (mode == GPIO_INPUT_FLOATING || mode == GPIO_OUTPUT_OPENDRAIN)
   {
      GPIO_SET_FLOATING_OPENDRAIN(handler->port, handler->pin);
   }
   else if (mode == GPIO_INPUT_PULLUP || mode == GPIO_OUTPUT_PUSHPULL)
   {
      GPIO_SET_PULLUP_PUSHPULL(handler->port, handler->pin);
   }
   else
   {
      result = STM_INVALID_GPIO_MODE;
   }

   return result;
}

stm_err_t gpio_set_isr_or_speed(gpio_handle_t* handler, gpio_isr_or_speed_t isr_or_speed)
{
   stm_err_t result = STM_OK;

   if(handler == NULL)
   {
      return STM_NULLPTR;
   }

   if (isr_or_speed == GPIO_INPUT_ISR_DISABLE || isr_or_speed == GPIO_OUTPUT_SPEED_2MHZ)
   {
      GPIO_SET_ISR_DISABLE_SPEED_2MHZ(handler->port, handler->pin);
   }
   else if (isr_or_speed == GPIO_INPUT_ISR_ENABLE || isr_or_speed == GPIO_OUTPUT_SPEED_10MHZ)
   {
      GPIO_SET_ISR_ENABLE_SPEED_10MHZ(handler->port, handler->pin);
   }
   else
   {
      result = STM_INVALID_GPIO_ISR_OR_SPEED;
   }

   return result;
}

stm_err_t gpio_toggle_pin(gpio_handle_t* handler)
{
   if(handler == NULL)
   {
      return STM_NULLPTR;
   }

   handler->port->ODR ^= 1U<<(handler->pin);

   return STM_OK;
}

stm_err_t gpio_set_level(gpio_handle_t* handler, gpio_level_t level)
{
   stm_err_t result = STM_OK;

   if(handler == NULL)
   {
      return STM_NULLPTR;
   }

   if (level == GPIO_LEVEL_HIGH)
   {
      GPIO_SET_PIN_HIGH(handler->port, handler->pin);
   }
   else if (level == GPIO_LEVEL_LOW)
   {
      GPIO_SET_PIN_LOW(handler->port, handler->pin);
   }
   else
   {
      result = STM_INVALID_GPIO_LEVEL;
   }

   return result;
}